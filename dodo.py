# SPDX-License-Identifier: GPL-2.0-or-later OR AGPL-3.0-or-later OR CERN-OHL-S-2.0+
import os, site, re
from os.path import basename
from pathlib import Path
from textwrap import dedent
import yaml
from xml.etree import ElementTree as ET
from typing import Collection, Dict, cast

from doit.tools import run_once

from pdkmaster.typing import GDSLayerSpecDict
from pdkmaster.task import get_var_env, OpenPDKTree
from pdkmaster.technology import technology_ as _tch
from pdkmaster.design import library as _lbry
from pdkmaster.io.klayout import TaskManager as KLayoutTaskManager
from pdkmaster.io.spice import SpiceNetlistFactory, SpicePrimsParamSpec, TaskManager as SpiceTaskManager
from pdkmaster.io.tasyag import LibertyCornerDataT, TaskManager as TasYagTaskManager
from pdkmaster.io.coriolis import TaskManager as CoriolisTaskManager

import pdkmaster.technology, pdkmaster.design, pdkmaster.dispatch
import pdkmaster.io.spice, pdkmaster.io.klayout
import c4m, c4m.flexcell, c4m.flexio


### Config


open_pdk_tasks = (
    "coriolis", "klayout", "spice_models", "spice", "gds",
    "rtl", "liberty",
)
DOIT_CONFIG = {
    "default_tasks": list(open_pdk_tasks),
}


### support functions and classes


def get_tech() -> _tch.Technology:
    from c4m.pdk import sky130
    return sky130.tech
def get_lib(lib_name: str) -> _lbry.Library:
    from c4m.pdk import sky130
    return cast(_lbry.Library, getattr(sky130, lib_name.lower()))
def get_spiceprims() -> SpicePrimsParamSpec:
    from c4m.pdk import sky130
    return sky130.prims_spiceparams
def get_netlistfab() -> SpiceNetlistFactory:
    from c4m.pdk import sky130
    return sky130.netlistfab
def get_gdslayers() -> GDSLayerSpecDict:
    from c4m.pdk import sky130
    return sky130.gds_layers


### globals


def _first(it):
    return next(iter(it))

top_dir = Path(__file__).parent
os.environ["TOP_DIR"] = str(top_dir)
tmp_dir = top_dir.joinpath("tmp")
scripts_dir = top_dir.joinpath("scripts")
dist_dir = top_dir.joinpath("dist")

override_dir = top_dir.joinpath("override")


# variables
pdkmaster_pip = get_var_env("pdkmaster_pip", default="pip3")
pdkmaster_python = get_var_env("pdkmaster_python", default="python3")
coriolis_python = get_var_env("coriolis_python", default="python2")
volare_rootdir = top_dir.joinpath(".volare")
# Set PDK_ROOT env. variable for volare
os.environ["PDK_ROOT"] = str(volare_rootdir)
sky130_pdk_dir = volare_rootdir.joinpath("sky130A")
sky130_pdk_tech_spice_dir = sky130_pdk_dir.joinpath("libs.tech", "ngspice")
sky130_pdk_ref_spice_dir = sky130_pdk_dir.joinpath("libs.ref", "sky130_fd_pr", "spice")

c4m_local_dir = top_dir.joinpath("c4m")
sky130_local_dir = c4m_local_dir.joinpath("pdk", "sky130")
# Don't use local module for c4m
c4m_inst_dir = Path(site.getsitepackages()[0]).joinpath("c4m")
sky130_inst_dir = c4m_inst_dir.joinpath("pdk", "sky130")
flexcell_inst_dir = Path(c4m.flexcell.__file__).parent
flexio_inst_dir = Path(c4m.flexio.__file__).parent

c4m_pdk_sky130_deps = (
    *sky130_local_dir.rglob("*.py"),
)
pdkmaster_deps = (
    *Path(_first(pdkmaster.technology.__path__)).rglob("*.py"),
    *Path(_first(pdkmaster.design.__path__)).rglob("*.py"),
    *Path(_first(pdkmaster.dispatch.__path__)).rglob("*.py"),
)
pdkmaster_io_spice_deps = (
    *Path(_first(pdkmaster.io.spice.__path__)).rglob("*.py"),
)
pdkmaster_io_klayout_deps = (
    *Path(_first(pdkmaster.io.klayout.__path__)).rglob("*.py"),
)
flexcell_deps = (
    *Path(_first(c4m.flexcell.__path__)).rglob("*.py"),
)
flexio_deps = (
    *Path(_first(c4m.flexio.__path__)).rglob("*.py"),
)


### cell list


cell_list_file = top_dir.joinpath("cell_list.yml")

def task_cell_list():
    """Regenerate cell list.

    This task is not run by default. It needs to be run manually when the cell list
    has been changed and then the updated file has to be commit to git.
    """
    def write_list():
        import yaml

        from c4m.pdk import sky130

        cell_list = {
            lib.name: list(cell.name for cell in lib.cells)
            for lib in sky130.libs
        }
        with cell_list_file.open("w") as f:
            yaml.dump(cell_list, f)

    return {
        "title": lambda _: "Creating cell list file",
        "targets": (
            cell_list_file,
        ),
        "actions": (
            write_list,
        ),
    }

# We assume that the cell list is stored in git and is available in the top directory.
assert cell_list_file.exists()
with cell_list_file.open("r") as f:
    cell_list: Dict[str, Collection[str]]
    cell_list = yaml.safe_load(f)

# Which python modules are used for the libraries
lib_deps = {
    "StdCellLib": (*pdkmaster_deps, *flexcell_deps),
    "StdCell5V0Lib": (*pdkmaster_deps, *flexcell_deps),
    "IOLib": (*pdkmaster_deps, *flexcell_deps, *flexio_deps),
    "MacroLib": (*pdkmaster_deps,),
}


### main tasks


#
# volare upstream Sky130 PDK
def task_volare():
    volare_hash = "bdc9412b3e468c102d01b7cf6337be06ec6e9c9a"

    return {
        "uptodate": (
            run_once,
        ),
        "actions": (
            f"volare enable --pdk sky130 -l sky130_fd_pr {volare_hash}",
        ),
        "targets": (
            volare_rootdir.joinpath("volare", "sky130", "versions", volare_hash),
        )
    }


#
# openpdk_tree and task
openpdk_tree = OpenPDKTree(top=top_dir.joinpath("open_pdk"), pdk_name="C4M.Sky130")

task_open_pdk_dir = openpdk_tree.task_func

def task_open_pdk():
    "All tasks to do for a completely fille open_pdk directory"
    return {
        "task_dep": open_pdk_tasks,
        "actions": (),
    }


#
# spice_models
open_pdk_spice_dir = openpdk_tree.tool_dir(tool_name="ngspice")
spice_corners = ("tt", "ff", "ss", "fs", "sf")
bip_spice_corners = ("t", "f", "s")
rc_spice_corners = ("tt", "ll", "hh", "lh", "hl")
rc_spice_files = {
    "tt": ("res_typical__cap_typical.spice", "res_typical__cap_typical__lin.spice"),
    "ll": ("res_low__cap_low.spice", "res_low__cap_low__lin.spice"),
    "hh": ("res_high__cap_high.spice", "res_high__cap_high__lin.spice"),
    "lh": ("res_low__cap_high.spice", "res_low__cap_high__lin.spice"),
    "hl": ("res_high__cap_low.spice", "res_high__cap_low__lin.spice"),
}
spice_model_files = {
    **{
        f"logic_{corner}": {
            "deps": (
                sky130_pdk_ref_spice_dir.joinpath(
                    f"sky130_fd_pr__nfet_01v8__{corner}.pm3.spice",
                ),
                sky130_pdk_ref_spice_dir.joinpath(
                    f"sky130_fd_pr__pfet_01v8__{corner}.pm3.spice",
                ),
                sky130_pdk_ref_spice_dir.joinpath(
                    f"sky130_fd_pr__nfet_01v8_lvt__{corner}.pm3.spice",
                ),
                sky130_pdk_ref_spice_dir.joinpath(
                    f"sky130_fd_pr__pfet_01v8_lvt__{corner}.pm3.spice",
                ),
                sky130_pdk_ref_spice_dir.joinpath(
                    f"sky130_fd_pr__pfet_01v8_hvt__{corner}.pm3.spice",
                ),
            ),
            "targets": (
                open_pdk_spice_dir.joinpath(f"C4M.Sky130_logic_{corner}_model.spice"),
            ),
        }
        for corner in spice_corners
    },
    **{
        f"io_{corner}": {
            "deps": (
                sky130_pdk_ref_spice_dir.joinpath(
                    f"sky130_fd_pr__nfet_g5v0d10v5__{corner}.pm3.spice",
                ),
                sky130_pdk_ref_spice_dir.joinpath(
                    f"sky130_fd_pr__pfet_g5v0d10v5__{corner}.pm3.spice",
                ),
            ),
            "targets": (
                open_pdk_spice_dir.joinpath(f"C4M.Sky130_io_{corner}_model.spice"),
            ),
        }
        for corner in spice_corners
    },
    "diode": {
        "deps": tuple(
            sky130_pdk_ref_spice_dir.joinpath(f)
            for f in (
                "sky130_fd_pr__diode_pw2nd_05v5.model.spice",
                "sky130_fd_pr__diode_pd2nw_05v5.model.spice",
                # "sky130_fd_pr__diode_pd2nw_05v5_hvt.model.spice",
                # "sky130_fd_pr__diode_pd2nw_05v5_lvt.model.spice",
                # "sky130_fd_pr__diode_pd2nw_11v0.model.spice",
                # "sky130_fd_pr__diode_pd2nw_11v0_no_rs.model.spice",
                # "sky130_fd_pr__diode_pw2nd_05v5__extended_drain.model.spice",
                # "sky130_fd_pr__diode_pw2nd_05v5_lvt.model.spice",
                # "sky130_fd_pr__diode_pw2nd_05v5_nvt.model.spice",
                # "sky130_fd_pr__diode_pw2nd_11v0.model.spice",
            )
        ),
        "targets": (
            open_pdk_spice_dir.joinpath(f"C4M.Sky130_diode_model.spice"),
        ),
    },
    **{
        f"diode_{corner}": {
            "deps": (
                sky130_pdk_tech_spice_dir.joinpath("corners", corner, "nonfet.spice"),
                sky130_pdk_ref_spice_dir.joinpath(
                    f"sky130_fd_pr__pfet_01v8__{corner}.corner.spice",
                )
            ),
            "targets": (
                open_pdk_spice_dir.joinpath(f"C4M.Sky130_diode_{corner}_params.spice"),
            ),
            "params": (
                "sky130_fd_pr__nfet_01v8__ajunction_mult",
                "sky130_fd_pr__nfet_01v8__pjunction_mult",
                "sky130_fd_pr__pfet_01v8__ajunction_mult",
                "sky130_fd_pr__pfet_01v8__pjunction_mult",
                "sky130_fd_pr__model__parasitic__diode_pw2dn__ajunction_mult",
                "sky130_fd_pr__model__parasitic__diode_ps2nw__ajunction_mult",
                "sky130_fd_pr__model__parasitic__diode_ps2dn__pjunction_mult",
                "sky130_fd_pr__model__parasitic__diode_ps2nw__pjunction_mult",
            )
        }
        for corner in spice_corners
    },
    "pnp": {
        "deps": tuple(
            sky130_pdk_ref_spice_dir.joinpath(f)
            for f in (
                "sky130_fd_pr__pnp_05v5_W0p68L0p68.model.spice",
                "sky130_fd_pr__pnp_05v5_W3p40L3p40.model.spice",
            )
        ),
        "targets": (
            open_pdk_spice_dir.joinpath(f"C4M.Sky130_pnp_model.spice"),
        ),
    },
    **{
        f"pnp_{corner}": {
            "deps": (
                sky130_pdk_tech_spice_dir.joinpath("corners", corner+corner, "nonfet.spice"),
            ),
            "targets": (
                open_pdk_spice_dir.joinpath(f"C4M.Sky130_pnp_{corner}_params.spice"),
            ),
            "params": (
                "dkispp", "dkbfpp", "dknfpp",
                "dkispp5x", "dkbfpp5x", "dknfpp5x",
            )
        }
        for corner in bip_spice_corners
    },
    "npn": {
        "deps": tuple(
            sky130_pdk_ref_spice_dir.joinpath(f)
            for f in (
                "sky130_fd_pr__npn_05v5_W1p00L1p00.model.spice",
                "sky130_fd_pr__npn_05v5_W1p00L2p00.model.spice",
            )
        ),
        "targets": (
            open_pdk_spice_dir.joinpath(f"C4M.Sky130_npn_model.spice"),
        ),
    },
    **{
        f"npn_{corner}": {
            "deps": (
                sky130_pdk_ref_spice_dir.joinpath(
                    f"sky130_fd_pr__npn_05v5__{corner}.corner.spice",
                ),
            ),
            "targets": (
                open_pdk_spice_dir.joinpath(f"C4M.Sky130_npn_{corner}_params.spice"),
            ),
            "params": (
                "dkisnpn1x1", "dkbfnpn1x1",
                "dkisnpn1x2", "dkbfnpn1x2",
                "dkisnpnpolyhv", "dkbfnpnpolyhv",
            )
        }
        for corner in bip_spice_corners
    },
    "rc_common": {
        "deps": (
            sky130_pdk_tech_spice_dir.joinpath("sky130_fd_pr__model__r+c.model.spice"),
        ),
        "targets": (
            open_pdk_spice_dir.joinpath("C4M.Sky130_rc_common_params.spice"),
        ),
        "params": (
            "tc1rsn", "tc2rsn", "tc1rsn_h", "tc2rsn_h", "nfom_dw",
            "tc1rsp", "tc2rsp", "tc1rsp_h", "tc2rsp_h", "pfom_dw",
            "tc1rsgpu", "tc2rsgpu", "poly_dw",
            "tc1rsnw", "tc2rsnw",
            "tc1rl1", "tc2rl1", "li_dw",
            "tc1rm1", "tc2rm1", "m1_dw",
            "tc1rvia", "tc2rvia",
            "tc1rm2", "tc2rm2", "m2_dw",
            "tc1rvia2", "tc2rvia2",
            "tc1rm3", "tc2rm3", "m3_dw",
            "tc1rvia3", "tc2rvia3",
            "tc1rm4", "tc2rm4", "m4_dw",
            "tc1rvia4", "tc2rvia4",
            "tc1rm5", "tc2rm5", "m5_dw",
            "tc1rrdl", "tc2rrdl", "rdl_dw",
        ),
    },
    **{
        f"rc_{corner}": {
            "deps": tuple(
                sky130_pdk_tech_spice_dir.joinpath(
                    "r+c", file_name,
                ) for file_name in rc_spice_files[corner]
            ),
            "targets": (
                open_pdk_spice_dir.joinpath(f"C4M.Sky130_rc_{corner}_params.spice"),
            ),
            "params": (
                "rdn", "rdn_hv", "tol_nfom",
                "rdp", "rdp_hv", "tol_pfom",
                "rp1", "tol_poly",
                "rcp1",
                "rnw", "tol_nw",
                "rl1", "tol_li",
                "rcl1",
                "rm1", "tol_m1",
                "rcvia",
                "rm2", "tol_m2",
                "rcvia2",
                "rm3", "tol_m3",
                "rcvia3",
                "camimc", "cpmimc",
                "rm4", "tol_m4",
                "rcvia4",
                "rm5", "tol_m5",
                "rcrdlcon",
                "rrdl", "tol_rdl",
            ),
        }
        for corner in rc_spice_corners
    },
    # TODO: Decide to split of MIM models in separate corner or not
    "rc_model": {
        "deps": (
            sky130_pdk_tech_spice_dir.joinpath("sky130_fd_pr__model__r+c.model.spice"),
        ),
        "targets": (
            open_pdk_spice_dir.joinpath("C4M.Sky130_rc_model.spice"),
        ),
        "models": (
            "sky130_fd_pr__res_generic_nd", "sky130_fd_pr__res_generic_nd__hv",
            "sky130_fd_pr__res_generic_pd", "sky130_fd_pr__res_generic_pd__hv",
            "sky130_fd_pr__res_generic_po",
            "sky130_fd_pr__res_generic_nw",
            "sky130_fd_pr__res_generic_l1",
            "sky130_fd_pr__res_generic_m1",
            "sky130_fd_pr__res_generic_m2",
            "sky130_fd_pr__res_generic_m3",
            "sky130_fd_pr__res_generic_m4",
            "sky130_fd_pr__res_generic_m5",
            "sky130_fd_pr__res_generic_rl",
        )
    },
    "mim_model": {
        "deps": (
            sky130_pdk_ref_spice_dir.joinpath("sky130_fd_pr__cap_mim_m3_1.model.spice"),
            sky130_pdk_ref_spice_dir.joinpath("sky130_fd_pr__cap_mim_m3_2.model.spice"),
        ),
        "targets": (
            open_pdk_spice_dir.joinpath("C4M.Sky130_mim_model.spice"),
        ),
    },
}
spice_logic_lib_file = open_pdk_spice_dir.joinpath(f"C4M.Sky130_logic_lib.spice")
spice_io_lib_file = open_pdk_spice_dir.joinpath(f"C4M.Sky130_io_lib.spice")
spice_diode_lib_file = open_pdk_spice_dir.joinpath(f"C4M.Sky130_diode_lib.spice")
spice_pnp_lib_file = open_pdk_spice_dir.joinpath(f"C4M.Sky130_pnp_lib.spice")
spice_npn_lib_file = open_pdk_spice_dir.joinpath(f"C4M.Sky130_npn_lib.spice")
spice_rc_lib_file = open_pdk_spice_dir.joinpath(f"C4M.Sky130_rc_lib.spice")
spice_all_lib_file = open_pdk_spice_dir.joinpath(f"C4M.Sky130_all_lib.spice")
spice_lib_file = open_pdk_spice_dir.joinpath(f"C4M.Sky130_lib.spice")
def task_spice_models():
    """Convert Sky130 spice model files

    The model files are converted as the open_pdk ones are not compatible with
    HiTas/Yagle."""
    def spice_model_title(task):
        corner = task.name[13:]
        return f"Converting spice model file for corner {corner}"

    def remove_semicolon(corner):
        open_pdk_spice_dir.mkdir(parents=True, exist_ok=True)

        files = spice_model_files[corner]
        with files["targets"][0].open("w") as f_target:
            for dep in files["deps"]:
                with dep.open("r") as f_dep:
                    for l in f_dep:
                        # Yagle does not like ; comments in multiline parameter list
                        if ";" in l:
                            l = l[:l.index(";")] + "\n"
                        f_target.write(l)

    def convert_spice(corner):
        import re

        open_pdk_spice_dir.mkdir(parents=True, exist_ok=True)

        files = spice_model_files[corner]
        with files["targets"][0].open("w") as f_target:
            for dep in files["deps"]:
                with dep.open("r") as f_dep:
                    # We start with ignoring the lines
                    output = False
                    for line in f_dep:
                        # Extract only the .model decks
                        if line.startswith(".model"):
                            output = True
                        if line.startswith(".ends"):
                            output = False
                        if output:
                            # Remove l/w parameter dependency.
                            # These are only used for Monte-Carlo simulation and thus so
                            # we don't support these type of simulations yet.
                            # It is removed as the l/w dependency is the cause of HiTas/Yagle
                            # incompatibility.
                            # l/w is always used in a {} section that starts with a float;
                            # we only take first float without the rest of the section.
                            s = re.search("{(-?\d+(\.\d*)?(e(-|\+)?\d+)?).*}", line) # type: ignore
                            if s:
                                line = line[:s.start()] + s.groups()[0] + line[s.end():]
                            f_target.write(line)

    def zero_mc_switch(corner):
        import re

        open_pdk_spice_dir.mkdir(parents=True, exist_ok=True)

        files = spice_model_files[corner]
        with files["targets"][0].open("w") as f_target:
            for dep in files["deps"]:
                with dep.open("r") as f_dep:
                    for line in f_dep:
                        s = re.search(
                            "MC_MM_SWITCH\*AGAUSS\([^\)]*\)((\*[^/]*)?/sqrt\([^\)]*\)|\*\([^\)]*\))",
                            line,
                        )
                        if s:
                            line = line[:s.start()] + "0.0" + line[s.end():]
                        if ";" in line:
                            line = line[:line.index(";")] + "\n"
                        f_target.write(line)

    def extract_params(corner):
        open_pdk_spice_dir.mkdir(parents=True, exist_ok=True)

        files = spice_model_files[corner]
        tgts = files["targets"]
        assert len(tgts) == 1
        with tgts[0].open("w") as f_target:
            print(f"* {corner}\n.param", file=f_target)
            for dep in files["deps"]:
                dep: Path
                with dep.open("r") as f_dep:
                    for l in f_dep:
                        for param in files["params"]:
                            l = re.sub(" +", " ", l)
                            l = re.sub(" *= *", "=", l)
                            l = l.replace(".param", "+")
                            if (
                                l.startswith(f"+ {param} =")
                                or l.startswith(f"+ {param}=")
                            ):
                                # Remove ; part
                                if ";" in l:
                                    l = l[:l.index(";")] + "\n"
                                f_target.write(l)

    def extract_models(corner):
        open_pdk_spice_dir.mkdir(parents=True, exist_ok=True)

        files = spice_model_files[corner]
        tgts = files["targets"]
        assert len(tgts) == 1
        with tgts[0].open("w") as f_target:
            print(f"* {corner}", file=f_target)
            for dep in files["deps"]:
                dep: Path
                with dep.open("r") as f_dep:
                    for l in f_dep:
                        l = re.sub(" +", " ", l)
                        l = re.sub(" *= *", "=", l)
                        ws = l.split()
                        if (len(ws) > 0) and (ws[0] == ".model"):
                            l = re.sub('={?"(?P<f>[^"]*)"}?', "={\g<f>}", l)
                            if ws[1] in files["models"]:
                                f_target.write(l)

    def write_libs():
        open_pdk_spice_dir.mkdir(parents=True, exist_ok=True)

        with spice_logic_lib_file.open("w") as f:
            print("* C4M.Sky130 logic transistors lib file\n", file=f)

            for corner in spice_corners:
                f.write(dedent(f"""
                    .lib {corner}
                    .include "C4M.Sky130_logic_{corner}_model.spice"
                    .endl {corner}
                """[1:]))

        with spice_io_lib_file.open("w") as f:
            print("* C4M.Sky130 IO transistors lib file\n", file=f)

            for corner in spice_corners:
                f.write(dedent(f"""
                    .lib {corner}
                    .include "C4M.Sky130_io_{corner}_model.spice"
                    .endl {corner}
                """[1:]))

        with spice_diode_lib_file.open("w") as f:
            print("* C4M.Sky130 diode lib file\n", file=f)

            for corner in spice_corners:
                f.write(dedent(f"""
                    .lib {corner}
                    .include "C4M.Sky130_diode_{corner}_params.spice"
                    .include "C4M.Sky130_diode_model.spice"
                    .endl {corner}
                """[1:]))

        with spice_pnp_lib_file.open("w") as f:
            print("* C4M.Sky130 pnp lib file\n", file=f)

            for corner in bip_spice_corners:
                f.write(dedent(f"""
                    .lib {corner}
                    .include "C4M.Sky130_pnp_{corner}_params.spice"
                    .include "C4M.Sky130_pnp_model.spice"
                    .endl {corner}
                """[1:]))

        with spice_npn_lib_file.open("w") as f:
            print("* C4M.Sky130 npn lib file\n", file=f)

            for corner in bip_spice_corners:
                f.write(dedent(f"""
                    * npn model needs also diode corner
                    .lib {corner}
                    .include "C4M.Sky130_npn_{corner}_params.spice"
                    .include "C4M.Sky130_npn_model.spice"
                    .endl {corner}
                """[1:]))

        with spice_rc_lib_file.open("w") as f:
            print("* C4M.Sky130 rc lib file\n", file=f)

            for corner in rc_spice_corners:
                f.write(dedent(f"""
                    .lib {corner}
                    .include "C4M.Sky130_rc_{corner}_params.spice"
                    .include "C4M.Sky130_rc_common_params.spice"
                    .include "C4M.Sky130_rc_model.spice"
                    .include "C4M.Sky130_mim_model.spice"
                    .endl {corner}
                """[1:]))

        with spice_all_lib_file.open("w") as f:
            print("* C4M.Sky130 global per device corner lib file\n", file=f)

            for corner in spice_corners:
                f.write(dedent(f"""
                    .lib logic_{corner}
                    .include "C4M.Sky130_logic_{corner}_model.spice"
                    .endl logic_{corner}
                """[1:]))

            for corner in spice_corners:
                f.write(dedent(f"""
                    .lib io_{corner}
                    .include "C4M.Sky130_io_{corner}_model.spice"
                    .endl io_{corner}
                """[1:]))

            for corner in spice_corners:
                f.write(dedent(f"""
                    .lib diode_{corner}
                    .include "C4M.Sky130_diode_{corner}_params.spice"
                    .include "C4M.Sky130_diode_model.spice"
                    .endl diode_{corner}
                """[1:]))

            for corner in bip_spice_corners:
                f.write(dedent(f"""
                    .lib pnp_{corner}
                    .include "C4M.Sky130_pnp_{corner}_params.spice"
                    .include "C4M.Sky130_pnp_model.spice"
                    .endl npn_{corner}
                """[1:]))

            for corner in bip_spice_corners:
                f.write(dedent(f"""
                    .lib npn_{corner}
                    .include "C4M.Sky130_npn_{corner}_params.spice"
                    .include "C4M.Sky130_npn_model.spice"
                    .endl npn_{corner}
                """[1:]))

            for corner in rc_spice_corners:
                f.write(dedent(f"""
                    .lib rc_{corner}
                    .include "C4M.Sky130_rc_{corner}_params.spice"
                    .include "C4M.Sky130_rc_common_params.spice"
                    .include "C4M.Sky130_rc_model.spice"
                    .include "C4M.Sky130_mim_model.spice"
                    .endl rc_{corner}
                """[1:]))

        with spice_lib_file.open("w") as f:
            print(
                "* deprecated C4M.Sky130 global lib file\n"
                "* Use the C4M.Sky130_all_lib.spice instead",
                file=f,)

            for corner in spice_corners:
                f.write(dedent(f"""
                    .lib {corner}
                    .include "C4M.Sky130_logic_{corner}_model.spice"
                    .include "C4M.Sky130_io_{corner}_model.spice"
                    .include "C4M.Sky130_diode_{corner}_params.spice"
                    .include "C4M.Sky130_diode_model.spice"
                    .endl {corner}
                """[1:]))

    corner_funcs = {
        **{f"logic_{corner}": convert_spice for corner in spice_corners},
        **{f"io_{corner}": convert_spice for corner in spice_corners},
        "diode": remove_semicolon,
        **{f"diode_{corner}": extract_params for corner in spice_corners},
        "pnp":zero_mc_switch,
        **{f"pnp_{corner}": extract_params for corner in bip_spice_corners},
        "npn": zero_mc_switch,
        **{f"npn_{corner}": extract_params for corner in bip_spice_corners},
        "rc_common": extract_params,
        "rc_model": extract_models,
        **{f"rc_{corner}": extract_params for corner in rc_spice_corners},
        "mim_model": zero_mc_switch,
    }

    for (corner, files) in spice_model_files.items():
        yield {
            "title": spice_model_title,
            "name": corner,
            "doc": f"Converting spice model file for corner {corner}",
            "task_dep": (
                "volare",
            ),
            "file_dep": files["deps"],
            "targets": files["targets"],
            "actions": (
                (corner_funcs[corner], (corner,)),
            )
        }
    yield {
        "title": lambda _: "Writing top lib files",
        "name": "lib",
        "doc": f"Writing lib files",
        "task_dep": (
            "volare",
        ),
        "uptodate": (
            run_once,
        ),
        "targets": (
            spice_logic_lib_file,
            spice_io_lib_file,
            spice_diode_lib_file,
            spice_pnp_lib_file,
            spice_npn_lib_file,
            spice_rc_lib_file,
            spice_all_lib_file,
            spice_lib_file,
        ),
        "actions": (
            write_libs,
        ),
    }


#
# spice_models_python (copy inside pytho module)
python_models_dir = sky130_local_dir.joinpath("models")
def _repl_models_dir():
    def _repl_dir(p: Path) -> Path:
        b = basename(str(p))
        return python_models_dir.joinpath(b)
    for spec in spice_model_files.values():
        for p in spec["targets"]:
            yield (p, _repl_dir(p))
    yield(spice_logic_lib_file, _repl_dir(spice_logic_lib_file))
    yield(spice_io_lib_file, _repl_dir(spice_io_lib_file))
    yield(spice_diode_lib_file, _repl_dir(spice_diode_lib_file))
    yield(spice_pnp_lib_file, _repl_dir(spice_pnp_lib_file))
    yield(spice_npn_lib_file, _repl_dir(spice_npn_lib_file))
    yield(spice_all_lib_file, _repl_dir(spice_all_lib_file))
    yield(spice_lib_file, _repl_dir(spice_lib_file))
python_models_srctgts = tuple(_repl_models_dir())
python_models_deps = tuple(scr for (scr, _) in python_models_srctgts)
python_models_tgts = tuple(tgt for (_, tgt) in python_models_srctgts)
def task_spice_models_python():
    """Copy SPICE models inside pdk module

    This way they can be used by pyspicefactory without needing separate
    PDK install"""
    return {
        "file_dep": python_models_deps,
        "targets": (
            *python_models_tgts,
        ),
        "actions": (
            f"mkdir -p {str(python_models_dir)}",
            *(f"cp {str(src)} {str(tgt)}" for src, tgt in python_models_srctgts),
        )
    }


#
# coriolis
def task_coriolis():
    """Generate coriolis support files"""

    coriolis_dir = openpdk_tree.tool_dir(tool_name="coriolis")
    corio_dir = coriolis_dir.joinpath("techno", "etc", "coriolis2")
    corio_node130_dir = corio_dir.joinpath("node130")
    corio_sky130_dir = corio_node130_dir.joinpath("sky130")

    corio_nda_init_file = corio_dir.joinpath("__init__.py")
    corio_node130_init_file = corio_node130_dir.joinpath("__init__.py")
    corio_sky130_init_file = corio_sky130_dir.joinpath("__init__.py")
    corio_sky130_techno_file = corio_sky130_dir.joinpath("techno.py")
    fix_libs = ("StdCellLib", "StdCell5V0Lib")
    corio_sky130_lib_files = tuple(
        corio_sky130_dir.joinpath(f"{lib}.py") for lib in cell_list.keys()
    ) + tuple(
        corio_sky130_dir.joinpath(f"{lib}_fix.py") for lib in fix_libs
    )

    def gen_init():
        from c4m.pdk import sky130

        corio_sky130_dir.mkdir(parents=True, exist_ok=True)

        corio_nda_init_file.touch()
        corio_node130_init_file.touch()
        with corio_sky130_init_file.open("w") as f:
            print("from .techno import *", file=f)
            for lib in sky130.libs:
                print(f"from .{lib.name} import setup as {lib.name}_setup", file=f)

            print(
                "\n__lib_setups__ = [{}]".format(
                    ",".join(f"{lib.name}.setup" for lib in sky130.libs)
                ),
                file=f,
            )

    def gen_coriolis():
        from pdkmaster.io import coriolis as _iocorio
        from c4m.flexcell import coriolis_export_spec
        from c4m.pdk import sky130

        expo = _iocorio.FileExporter(
            tech=sky130.tech, gds_layers=sky130.gds_layers, spec=coriolis_export_spec,
        )

        with corio_sky130_techno_file.open("w") as f:
            f.write(dedent("""
                # Autogenerated file
                # SPDX-License-Identifier: GPL-2.0-or-later OR AGPL-3.0-or-later OR CERN-OHL-S-2.0+
            """))
            f.write(expo())

        for lib in sky130.libs:
            with corio_sky130_dir.joinpath(f"{lib.name}.py").open("w") as f:
                f.write(expo(lib, incl_cell=(lambda cell: cell.name != "Gallery")))
            if lib.name in fix_libs:
                with corio_sky130_dir.joinpath(f"{lib.name}_fix.py").open("w") as f:
                    f.write(dedent(f"""
                        import os
                        from pathlib import Path

                        from coriolis.CRL import Spice

                        def fix(lib):
                            spiceDir = Path(__file__).parents[7] / 'libs.ref' / '{lib.name}' / 'spice'
                            for spiceFile in os.listdir(spiceDir):
                                if spiceFile in ('{lib.name}.spi', "Gallery.spi):
                                    continue
                                if not spiceFile.endswith('.spi'):
                                    continue
                                Spice.load( lib, str(spiceDir / spiceFile), Spice.PIN_ORDERING )
                    """[1:]))

    return {
        "title": lambda _: "Creating coriolis files",
        "file_dep": (
            *c4m_pdk_sky130_deps,
            *pdkmaster_deps, *flexcell_deps, *flexio_deps,
        ),
        "targets": (
            corio_nda_init_file, corio_node130_init_file, corio_sky130_init_file,
            corio_sky130_techno_file, *corio_sky130_lib_files,
        ),
        "actions": (
            gen_init,
            gen_coriolis,
        ),
    }


#
# spice task manager and tasks
spice_tm = SpiceTaskManager(
    tech_cb=get_tech, lib4name_cb=get_lib, cell_list=cell_list,
    top_dir=top_dir, openpdk_tree=openpdk_tree, netlistfab_cb=get_netlistfab,
)

spice_export_task = spice_tm.create_export_task(
    extra_filedep=c4m_pdk_sky130_deps, extra_filedep_lib=lib_deps,
)
task_spice = spice_export_task.task_func


#
# klayout task manager and tasks
klayout_tm = KLayoutTaskManager(
    tech_cb=get_tech, lib4name_cb=get_lib, cell_list=cell_list,
    top_dir=top_dir, openpdk_tree=openpdk_tree, gdslayers_cb=get_gdslayers,
)

klayout_dir = openpdk_tree.tool_dir(tool_name="klayout")
klayout_tech_dir = klayout_dir.joinpath("tech", "C4M.Sky130")
klayout_bin_dir = klayout_dir.joinpath("bin")
def task_copy_convscript():
    """Copy the difftap conversion script"""
    name = "conv_c4msky130_to_sky130.py"
    script_src = scripts_dir.joinpath(name)
    script_tgt = klayout_bin_dir.joinpath(name)

    return {
        "title": lambda _: "Copy difftap conversion script",
        "file_dep": (
            script_src,
        ),
        "targets": (
            script_tgt,
        ),
        "actions": (
            f"mkdir -p {str(klayout_bin_dir)}; cp {str(script_src)} {str(script_tgt)}",
        ),
    }
def task_copy_lyp():
    """Copy the upstream layer properties file"""
    src = sky130_pdk_dir.joinpath("libs.tech", "klayout", "tech", "sky130A.lyp")
    tgt = klayout_tech_dir.joinpath("sky130.lyp")

    def cp_lyp() -> None:
        klayout_tech_dir.mkdir(parents=True, exist_ok=True)

        with src.open("r") as fin:
            with tgt.open("w") as fout:
                for line in fin:
                    fout.write(line.replace("diff.", "difftap."))

    return {
        "title": lambda _: "Copy the upstream layer properties file",
        "task_dep": (
            "volare",
        ),
        "file_dep": (
            src,
        ),
        "targets": (
            tgt,
        ),
        "actions": (
            cp_lyp,
        ),
    }


# klayout export
klayout_export_task = klayout_tm.create_export_task(
    spice_params_cb=get_spiceprims, layerprops_filename="sky130.lyp",
    extra_filedep=(*c4m_pdk_sky130_deps, *pdkmaster_deps),
    extra_taskdep=("copy_convscript", "copy_lyp")
)
task_klayout = klayout_export_task.task_func


# gds export
klayout_gds_task = klayout_tm.create_gds_task(
    extra_filedep=c4m_pdk_sky130_deps, extra_filedep_lib=lib_deps,
)
task_gds = klayout_gds_task.task_func_gds


# drc
def waive_macrodrc(lib: str, _: str, cat: ET.Element) -> bool:
    # Waive licon difftap enclosure rule in MacroLib
    # We use larger tap licon enclosure rule but these macros use the smaller
    # difftap licon enclosure rule.
    return (lib == "MacroLib") and (cat.text == "'difftap:licon asymmetric enclosure'")

klayout_drc_task = klayout_tm.create_drc_task(waive_func=waive_macrodrc)
task_drc = klayout_drc_task.task_func_drc
task_drccells = klayout_drc_task.task_func_drccells


# lvs
klayout_lvs_task = klayout_tm.create_lvs_task()
task_lvs = klayout_lvs_task.task_func_lvs
task_lvscells = klayout_lvs_task.task_func_lvscells


# sign-off
task_signoff = klayout_tm.task_func_signoff


#
# HiTAS/Yagle task manager and tasks
def tasyag_include_cell(lib: str, cell: str):
    return (
        (cell != "Gallery")
        and ((lib != "IOLib") or cell.startswith("IOPad"))
    )
tasyag_cell_list: Dict[str, Collection[str]] = {
    lib: tuple(filter(lambda cell: tasyag_include_cell(lib, cell), cells))
    for lib, cells in cell_list.items()
}

tasyag_tm = TasYagTaskManager(
    tech_cb=get_tech, lib4name_cb=get_lib, cell_list=tasyag_cell_list,
    top_dir=top_dir, openpdk_tree=openpdk_tree,
)
tasyag_spice_model_files = (
    open_pdk_spice_dir.joinpath("C4M.Sky130_logic_tt_model.spice"),
    open_pdk_spice_dir.joinpath("C4M.Sky130_io_tt_model.spice"),
    open_pdk_spice_dir.joinpath("C4M.Sky130_diode_tt_params.spice"),
    open_pdk_spice_dir.joinpath("C4M.Sky130_diode_model.spice"),
)


# rtl
rtl_libs = tuple(filter(lambda l: l not in ("ExampleSRAMs", "MacroLib"), cell_list.keys()))
rtl_tm = tasyag_tm.create_rtl_task(
    work_dir=tmp_dir, override_dir=override_dir, libs=rtl_libs,
    spice_model_files=tasyag_spice_model_files,
)
task_rtl = rtl_tm.task_func


# liberty
liberty_corner_data: LibertyCornerDataT = {
    "StdCellLib": {
        "nom": (1.80, 25, (open_pdk_spice_dir.joinpath("C4M.Sky130_logic_tt_model.spice"),)),
        "fast": (1.98, -20, (open_pdk_spice_dir.joinpath("C4M.Sky130_logic_ff_model.spice"),)),
        "slow": (1.62, 85, (open_pdk_spice_dir.joinpath("C4M.Sky130_logic_ss_model.spice"),)),
    },
    "StdCell5V0Lib": {
        "nom": (5.00, 25, (open_pdk_spice_dir.joinpath("C4M.Sky130_io_tt_model.spice"),)),
        "fast": (5.50, -20, (open_pdk_spice_dir.joinpath("C4M.Sky130_io_ff_model.spice"),)),
        "slow": (4.50, 85, (open_pdk_spice_dir.joinpath("C4M.Sky130_io_ss_model.spice"),)),
    },
}

liberty_tm = tasyag_tm.create_liberty_task(
    work_dir=tmp_dir, corner_data=liberty_corner_data,
)
task_liberty = liberty_tm.task_func


#
# release
def task_tarball():
    """Create a tarball"""
    from datetime import datetime

    tarballs_dir = top_dir.joinpath("tarballs")
    t = datetime.now()
    tarball = tarballs_dir.joinpath(f'{t.strftime("%Y%m%d_%H%M")}_c4m_pdk_sky130.tgz')

    return {
        "title": lambda _: "Create release tarball",
        "task_dep": (
            "coriolis", "klayout", "spice_models", "spice", "gds", "rtl", "liberty",
        ),
        "targets": (tarball,),
        "actions": (
            f"mkdir -p {str(tarballs_dir)}; cd {str(top_dir)}; tar czf {str(tarball)} open_pdk",
        )
    }
def task_tarball_nodep():
    """Create a tarball"""
    from datetime import datetime

    tarballs_dir = top_dir.joinpath("tarballs")
    t = datetime.now()
    tarball = tarballs_dir.joinpath(f'{t.strftime("%Y%m%d_%H%M")}_nd_c4m_pdk_sky130.tgz')

    return {
        "title": lambda _: "Create release tarball",
        "targets": (tarball,),
        "actions": (
            f"mkdir -p {str(tarballs_dir)}; cd {str(top_dir)}; tar czf {str(tarball)} open_pdk",
        )
    }


#
# Copy open_pdk into upstream repo
coriolis_tm = CoriolisTaskManager(
    tech_cb=get_tech, lib4name_cb=get_lib, cell_list=cell_list,
    top_dir=top_dir, openpdk_tree=openpdk_tree,
)

coriolis_cppdk_task = coriolis_tm.create_upstreampdk_task(
    alliancectk_co_dir=top_dir.joinpath("upstream", "alliance-check-toolkit"), openpdk_task="open_pdk",
)
task_coriolis_cp_pdk = coriolis_cppdk_task.task_func
