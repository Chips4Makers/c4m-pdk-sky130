#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later OR GPL-2.0-or-later OR CERN-OHL-S-2.0+ OR Apache-2.0
upstream_dir=$TOP_DIR/upstream/alliance-check-toolkit/pdkmaster
openpdk_dir=$TOP_DIR/open_pdk

ls $upstream_dir
if [ ! -d $upstream_dir ];
then
  echo "git submodule 'upstream/alliance-check-toolkit' is not initialized"
  exit 20
fi

# Get latest upstream main branch
cd $TOP_DIR
git submodule update --remote upstream/alliance-check-toolkit

# Switch to own branch
cd $upstream_dir
git checkout -B upstream_patch remotes/origin/main

# Replace files with latest version
rm -fr C4M.Sky130
cp -ar $openpdk_dir/C4M.Sky130 .
git add C4M.Sky130

# Commit
git commit -m "Patch for upstream"
